﻿using UnityEngine;

public class TestCitizenMove : MonoBehaviour
{
    [SerializeField]
    float _speed = 3;
    [SerializeField]
    Vector3[] _points;
    int _targetId;

    static STG.Camera.CameraController _camera;

    // Start is called before the first frame update
    void Start()
    {
        if (_points.Length == 0)
        {
            Destroy(this);
            return;
        }

        var y = transform.position.y;
        for (int i = 0; i < _points.Length; i++)
        {
            var p = _points[i];
            p.y = y;
            _points[i] = p;
        }

        if (_camera == null)
            _camera = FindObjectOfType<STG.Camera.CameraController>();
    }

    // Update is called once per frame
    void Update()
    {
        var step = Time.deltaTime * _speed;
        var distance = _points[_targetId] - transform.position;
        if (distance.magnitude < step)
        {
            transform.position = _points[_targetId++];
            if (_targetId == _points.Length)
                _targetId = 0;
            transform.LookAt(_points[_targetId]);
        }
        else
        {
            transform.position += distance.normalized * step;
        }
    }

    private void OnMouseUp()
    {
        if (_camera != null)
            _camera.FollowTransform(transform);
    }
}
