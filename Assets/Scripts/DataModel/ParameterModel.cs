﻿using STG.IncomeData;
using System;

namespace STG.DataModel
{
    public class ParameterModel : IParameterModel
    {
        public int Value { get; }

        public Guid Id { get; }

        public string Name { get; }

        public ParameterModel(ParameterData incomeData, LocalData.LocalData localData, int value)
        {
            if (incomeData == null)
            {
                Id = Guid.Empty;
            }
            else
            {
                Id = incomeData.Guid;
            }

            if (localData == null)
            {
                Name = string.Empty;
            }
            else
            {
                Name = localData.Name;
            }

            Value = value;
        }
    }
}
