﻿using STG.IncomeData;
using STG.LocalData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace STG.DataModel
{
    public class PlayerModel : IPlayerModel
    {
        public IDictionary<Guid, ITrophyModel> Trophies { get; private set; }

        public ITrophyModel CurrentTrophy
        {
            get
            {
                return Trophies[_data.CurentTrophyGuid];
            }
            set
            {
                _data.CurentTrophyGuid = value.Id;
            }
        }

        PlayerData _data;

        public void SetCurrentTrophyById(Guid trophyId)
        {
            throw new NotImplementedException();
        }

        public PlayerModel(IIncomeDataProvider incomeData, LocalDataProvider localData)
        {
            Trophies = incomeData.Trophies.ToDictionary(t => t.Key,
                t => new TrophyModel(t.Value, incomeData.Parameters, localData) as ITrophyModel);
            _data = incomeData.Player;
        }

    }
}
