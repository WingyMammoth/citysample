﻿using System;
using System.Collections.Generic;
using STG.IncomeData;
using Zenject;

namespace STG.DataModel
{
    public class ModelProvider : IModelProvider
    {
        public IPlayerModel Player { get; }

        public IDictionary<Guid, LevelModel> Levels { get; }
        public IDictionary<Guid, CitizenScheduleSettings> CitizenSchedules { get; }

        [Inject]
        public ModelProvider(IIncomeDataProvider incomeData, LocalData.LocalDataProvider localData)
        {
            Player = new PlayerModel(incomeData, localData);
            Levels = new Dictionary<Guid, LevelModel>(incomeData.Levels.Count);
            foreach (var data in incomeData.Levels)
                Levels.Add(data.Key, new LevelModel(data.Value, incomeData));
            CitizenSchedules = incomeData.CitizenSchedules;
            var schedules = incomeData.CitizenSchedules;
        }
    }
}
