﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// </summary>
namespace STG.DataModel
{
    public interface IModelProvider
    {
        IPlayerModel Player { get; }

        IDictionary<Guid, LevelModel> Levels { get; }
    }

    public interface IPlayerModel
    {
        IDictionary<Guid, ITrophyModel> Trophies { get; }
        //IEnumerable<ITrophyModel> ActiveTrophies { get; }

        ITrophyModel CurrentTrophy { get; set; }

        //void ReplaceTrophy(string activeTrophyId, string newActiveTrophyId);
        void SetCurrentTrophyById(Guid trophyId);
    }

    public interface IModel
    {
        Guid Id { get; }
        string Name { get; }
    }

    public interface IParameterModel : IModel
    {
        int Value { get; }
    }

    public interface ITrophyModel : IModel
    {
        IEnumerable<IParameterModel> Parameters { get; }
        IParameterModel GetParameter(Guid id);
        string Description { get; }
        Sprite Icon { get; }
    }

    public interface ILevelModel: IModel
    {
        int CitizenAmount { get; }
        IncomeData.CitizenScheduleSettings CitizenScheduleSettings { get; }
    }
}