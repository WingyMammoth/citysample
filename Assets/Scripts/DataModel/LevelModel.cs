﻿using System;
using System.Collections;
using System.Collections.Generic;
using STG.IncomeData;
using UnityEngine;

namespace STG.DataModel
{
    public class LevelModel : ILevelModel
    {
        public int CitizenAmount { get; }

        public CitizenScheduleSettings CitizenScheduleSettings { get; }

        public Guid Id { get; }

        public string Name { get; }

        public LevelModel(LevelData levelData, IIncomeDataProvider incomeData)
        {
            CitizenAmount = levelData.CitizenAmount;
            var settingsGuid = Guid.Parse(levelData.CitizenScheduleSettingsId);
            if (incomeData.CitizenSchedules.ContainsKey(settingsGuid))
                CitizenScheduleSettings = incomeData.CitizenSchedules[settingsGuid];
            Id = Guid.Parse(levelData.Id);
            Name = string.Empty; //TODO: take from local data

        }
    }
}