﻿using STG.IncomeData;
using STG.LocalData;
using System;
using System.Collections.Generic;

namespace STG.DataModel
{
    [Serializable]
    class TrophyModel : ITrophyModel
    {
        TrophyData _trophyData;
        LocalData.LocalData _localData;
        IParameterModel[] _parameters;

        public IEnumerable<IParameterModel> Parameters => _parameters;

        public string Description => _localData.Description;

        public Guid Id { get; }

        public string Name => _localData.Name;
        public UnityEngine.Sprite Icon => _localData.Icon;

        public TrophyModel(TrophyData data, IDictionary<Guid, ParameterData> parameters, LocalDataProvider localDataProvider)
        {
            _trophyData = data == null ? new TrophyData() : data;
            Id = Guid.Parse(_trophyData.Id);
            if (!localDataProvider.TryGetValue(Id, out _localData))
            {
                _localData = new LocalData.LocalData(string.Empty, string.Empty);
            }
            if (_trophyData.ParameterValues != null)
            {
                _parameters = new IParameterModel[_trophyData.ParameterValues.Length];
                int i = 0;
                foreach (var pv in _trophyData.ParameterValues)
                {
                    LocalData.LocalData pld = null;
                    localDataProvider.TryGetValue(pv.Guid, out pld);
                    ParameterData pd = null;
                    parameters.TryGetValue(pv.Guid, out pd);
                    _parameters[i] = new ParameterModel(pd, pld, pv.Value);
                    i++;
                }
            }
        }

        public IParameterModel GetParameter(Guid id)
        {
            foreach (var p in _parameters)
                if (p.Id == id)
                    return p;
            return null;
        }
    }
}
