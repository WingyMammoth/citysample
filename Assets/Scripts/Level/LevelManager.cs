﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace STG.Level
{
    public class LevelManager : ILevelManager, ITickable
    {
        [Inject]
        DataModel.IModelProvider _modelProvider;
        [Inject]
        ILevelCreator _creator;
        [Inject]
        Navigation.INavigationSystem _navigationSystem;
        [Inject]
        IScheduleSystem _scheduleSystem;
        [Inject]
        IHangoutSystem _hagoutSystem;

        ICitizenSystem[] _systems;

        bool _notActive = true;
        SceneData _sceneData;
        System.Random _random = new System.Random();

        public void InitializeIfNeeded()
        {
            if (_systems == null)
                _systems = new ICitizenSystem[]
                {
                    _navigationSystem as ICitizenSystem,
                    _scheduleSystem as ICitizenSystem
                };

            _scheduleSystem.OnScheduleItemStart += OnCitizenScheduleItemStartHandler;

            _navigationSystem.OnTargetPositionAchieved += _scheduleSystem.OnTargetPositionAchievedHandler;
            _navigationSystem.OnTargetPositionAchieved += _hagoutSystem.OnTargetPositionAchievedHandler;
        }

        public void LoadLevel()
        {
            InitializeIfNeeded();

            //HACK: first level hardcode - remake for LoadLevel(int levelIndex)
            var enumerator = _modelProvider.Levels.GetEnumerator();
            if (!enumerator.MoveNext())
            {
                return;
            }
            var level = enumerator.Current.Value;
            _sceneData = _creator.Create(level);
            _notActive = false;
        }

        public void Tick()
        {
            if (_notActive)
                return;
            var deltaTime = Time.deltaTime;
            for (int i = 0; i < _systems.Length; i++)
                _systems[i].Tick(deltaTime);
        }

        void OnCitizenScheduleItemStartHandler(ref CitizenFullComponent citizen)
        {
            switch (citizen.ScheduleItem)
            {
                case CitizenScheduleItemType.RoadToWork:
                    citizen.TargetPosition = _sceneData.Offices[_random.Next(_sceneData.Offices.Length)];
                    _navigationSystem.ValidateTargetPosition(ref citizen);
                    citizen.HideAfterPositionAchieved = true;
                    break;
                case CitizenScheduleItemType.RoadToHome:
                    citizen.TargetPosition = _sceneData.Houses[_random.Next(_sceneData.Houses.Length)];
                    _navigationSystem.ValidateTargetPosition(ref citizen);
                    citizen.HideAfterPositionAchieved = true;
                    break;
                default:
                    citizen.HideAfterPositionAchieved = false;
                    break;

            }
        }
    }
}