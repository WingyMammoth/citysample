﻿using UnityEngine;
using System.Collections;

namespace STG.Level
{
    internal class CitizenScheduleItem
    {
        public CitizenScheduleItemType Type;
        public int ExecutionMinutesTime;
    }

    internal class CitizenSchedule
    {
        CitizenScheduleItem[] _items;
        int _currentId = -1;

        //TODO: decide if core should not know about IncomeData namespace
        //Maybe app budiness logic part should know only about STG.DataModel namespace
        public CitizenSchedule(IncomeData.CitizenScheduleSettings settings)
        {
            //TODO: maybe schedules sould be generated on server side -
            //what means they are also related to IncomeData
            //so random generation on client is a temporary solution
            var random = new System.Random();
            int breakCount = random.Next(settings.BreaksCountMin, settings.BreaksCountMax + 1);

            //schedule:
            //home
            //road to work
            //work and break repeat breakCount times
            //work
            //hangout
            //roadToHome
            //so schedule contains 5 + 2 * breakCount elements
            _items = new CitizenScheduleItem[5 + 3 * breakCount];
            
            _items[1] = new CitizenScheduleItem() { Type = CitizenScheduleItemType.RoadToWork, ExecutionMinutesTime = 60 };

            int restWorkTime = random.Next(settings.WorkTimeMinutesMin, settings.WorkTimeMinutesMax + 1);
            int hangoutTime = random.Next(settings.HangoutTimeMinutesMin, settings.HangoutTimeMinutesMax + 1);
            int totalTime = restWorkTime + hangoutTime + 120; //120 minutes or 2 hours for roadToWork and roadToHome

            for (int i = 0; i < breakCount; i++)
            {
                float workTimePiece = System.Math.Min(.5f, (float)random.NextDouble());
                int workTime = (int)(workTimePiece * restWorkTime );
                restWorkTime -= workTime;
                int breakTime = random.Next(settings.BreakTimeMinutesMin, settings.BreakTimeMinutesMax + 1);
                totalTime += breakTime;
                _items[2 + i * 3] = new CitizenScheduleItem { Type = CitizenScheduleItemType.Work, ExecutionMinutesTime = workTime };
                _items[3 + i * 3] = new CitizenScheduleItem { Type = CitizenScheduleItemType.Hangout, ExecutionMinutesTime = breakTime };
                _items[4 + i * 3] = new CitizenScheduleItem() { Type = CitizenScheduleItemType.RoadToWork, ExecutionMinutesTime = 60 };
            }
            _items[2 + breakCount * 3] = new CitizenScheduleItem { Type = CitizenScheduleItemType.Work, ExecutionMinutesTime = restWorkTime };
            _items[3 + breakCount * 3] = new CitizenScheduleItem { Type = CitizenScheduleItemType.Hangout, ExecutionMinutesTime = hangoutTime };
            _items[4 + breakCount * 3] = new CitizenScheduleItem { Type = CitizenScheduleItemType.RoadToHome, ExecutionMinutesTime = 60 };
            
            _items[0] = new CitizenScheduleItem() { Type = CitizenScheduleItemType.Home, ExecutionMinutesTime = 24 * 60 - totalTime };
            
            float realSecondsInVirtualMinute = 1f / settings.VirtualMinutesInRealSecond;
            for (int i = 0; i < _items.Length; i++)
                _items[i].ExecutionMinutesTime = (int)(_items[i].ExecutionMinutesTime * realSecondsInVirtualMinute);
        }

        public CitizenScheduleItem SwitchToNextScheduleItem()
        {
            if (_items == null || _items.Length == 0)
                return default;
            _currentId++;
            if (_currentId == _items.Length)
                _currentId = 0;
            return _items[_currentId];
        }

        //public CitizenScheduleItem CurrentScheduleItem =>
        //    _items == null || _items.Length == 0 || _currentId < 0 ? default : _items[_currentId];

        public void AddTimeToFreeTime(float time)
        {
            AddTimeToFreeTime((int)time);
        }

        public void AddTimeToFreeTime(int time)
        {
            //last - road to home
            //free time - pre-last
            _items[_items.Length - 2].ExecutionMinutesTime += time;
        }
    }
}