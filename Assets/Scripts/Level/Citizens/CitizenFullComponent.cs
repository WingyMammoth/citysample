﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STG.Level
{
    partial struct CitizenFullComponent
    {
        public System.Guid Id;
        public float CurrentScheduleItemTimeLeft;
        public bool HideAfterPositionAchieved;
        public CitizenScheduleItemType ScheduleItem;
    }
}