﻿using STG.IncomeData;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace STG.Level
{
    enum CitizenScheduleItemType { Home, RoadToWork, Work, Hangout, RoadToHome}

    interface IScheduleSystem : ICitizenSystem, ITargetPositionchievedObserver
    {
        event ActionRef<CitizenFullComponent> OnScheduleItemStart;
        void Initialize(CitizenFullComponent[] citizens, CitizenScheduleSettings settings);
    }

    interface ITargetPositionchievedObserver
    { 
        void OnTargetPositionAchievedHandler(ref CitizenFullComponent citizen);
    }

    interface IActivatable
    {
        //bool Active { get; set; }
    }

    interface ICitizenSystem
    {
        
        void Tick(float deltaTime);
    }

    interface IHangoutSystem: IActivatable, ITargetPositionchievedObserver, IScheduleItemStartObserver
    {
    }

    interface IScheduleItemStartObserver
    {
        void OnCitizenScheduleItemStartHandler(ref CitizenFullComponent citizen);
    }
}