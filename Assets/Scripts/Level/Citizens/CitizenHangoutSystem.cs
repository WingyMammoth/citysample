﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STG.Level
{
    class CitizenHangoutSystem : IHangoutSystem
    {
        public void Initialize(CitizenFullComponent[] citizens)
        {
            //throw new System.NotImplementedException();
        }

        public void Tick(float deltaTime)
        {
            //throw new System.NotImplementedException();
        }

        public void OnTargetPositionAchievedHandler(ref CitizenFullComponent citizen)
        {
            Debug.Log("Position achieved");
            if (citizen.ScheduleItem != CitizenScheduleItemType.Hangout)
                return;
            
            SetNewTargetPosition(ref citizen);
        }

        public void OnCitizenScheduleItemStartHandler(ref CitizenFullComponent citizen)
        {
            Debug.Log("schedule start " + citizen.ScheduleItem);
            if (citizen.ScheduleItem != CitizenScheduleItemType.Hangout)
                return;
            citizen.Transform.gameObject.SetActive(true);
            SetNewTargetPosition(ref citizen);
        }

        //TODO: put al the constants to settings
        void SetNewTargetPosition(ref CitizenFullComponent citizen)
        {
            citizen.SetNewAgentTargetPosition(10, 2, -100, 300, -100, 100);
            citizen.TargetPosition = citizen.Agent.pathEndPosition;
        }
    }
}