﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STG.Level
{
    static class CitizenExtensions
    {
        static System.Random _random = new System.Random();

        public static void SetNewAgentTargetPosition(this ref CitizenFullComponent citizen,
            int triesMax, float deltaMin, int offsetXMin, int offsetXMax, int offsetZMin, int offsetZMax)
        {
            float delta = 0;
            while (delta < deltaMin && triesMax > 0)
            {
                triesMax--;
                var targetPoint = citizen.Transform.position
                    + new Vector3(_random.Next(offsetXMin, offsetXMax), 0, _random.Next(offsetZMin, offsetZMax));
                citizen.Agent.CalculatePath(targetPoint, citizen.Agent.path);
                citizen.Agent.SetPath(citizen.Agent.path); //TODO: check if it's necessary
                delta = (citizen.Transform.position - citizen.Agent.destination).magnitude;
            }
        }
    }
}