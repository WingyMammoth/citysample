﻿using System;
using System.Collections;
using System.Collections.Generic;
using STG.IncomeData;
using UnityEngine;

namespace STG.Level
{
    class CitizenScheduleSystem : IScheduleSystem
    {
        public bool Active { get; set; }
        public event ActionRef<CitizenFullComponent> OnScheduleItemStart;
        Dictionary<Guid, CitizenSchedule> _schedules;
        CitizenFullComponent[] _citizens;

        public void Initialize(CitizenFullComponent[] citizens, CitizenScheduleSettings settings)
        {
            var random = new System.Random();
            _schedules = new Dictionary<Guid, CitizenSchedule>(citizens.Length);
            _citizens = citizens;
            for (int i = 0; i < citizens.Length; i++)
            {
                var schedule = new CitizenSchedule(settings);
                _schedules.Add(citizens[i].Id, schedule);
                schedule.SwitchToNextScheduleItem();
                citizens[i].CurrentScheduleItemTimeLeft = (float)random.NextDouble() + random.Next(settings.StartDelayMinutesMax);
            }
        }

        public void InitializeComponents(CitizenFullComponent[] citizens)
        {
            //throw new NotImplementedException();
        }

        public void Tick(float deltaTime)
        {
            for (int i = 0; i < _citizens.Length; i++)
            {
                _citizens[i].CurrentScheduleItemTimeLeft -= deltaTime;

                if (_citizens[i].CurrentScheduleItemTimeLeft < 0)
                {
                    switch (_citizens[i].ScheduleItem)
                    {
                        case CitizenScheduleItemType.RoadToHome:
                        case CitizenScheduleItemType.RoadToWork:
                            break;
                        default:
                            Debug.Log("Schedule time left " + _citizens[i].ScheduleItem);

                            var scheduleItem = ExecuteNextScheduleItem(ref _citizens[i]);
                            //TODO: thnk about it
                            _citizens[i].Transform.gameObject.SetActive(true);
                            break;
                    }
                }
            }
        }

        CitizenScheduleItem ExecuteNextScheduleItem(ref CitizenFullComponent citizen)
        {
            var scheduleItem = _schedules[citizen.Id].SwitchToNextScheduleItem();
            Debug.Log(" Switch schedule from " + citizen.ScheduleItem + "  to  " + scheduleItem.Type);
            citizen.CurrentScheduleItemTimeLeft = scheduleItem.ExecutionMinutesTime;
            citizen.ScheduleItem = scheduleItem.Type;
            OnScheduleItemStart?.Invoke(ref citizen);
            return scheduleItem;
        }

        public void OnTargetPositionAchievedHandler(ref CitizenFullComponent citizen)
        {
            Debug.Log("Position achieved  hide = " + citizen.HideAfterPositionAchieved);
            if (citizen.HideAfterPositionAchieved)
                citizen.Transform.gameObject.SetActive(false);

            switch (citizen.ScheduleItem)
            {
                case CitizenScheduleItemType.RoadToHome:
                case CitizenScheduleItemType.RoadToWork:
                    _schedules[citizen.Id].AddTimeToFreeTime(citizen.CurrentScheduleItemTimeLeft);
                    ExecuteNextScheduleItem(ref citizen);
                    break;
                default:
                    break;
            }

        }
    }
}