﻿using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using System.Linq;
using Zenject;

namespace STG.Level
{
    interface ILevelCreator
    {
        SceneData Create(DataModel.LevelModel levelModel);
    }

    public class LevelCreatorECS
    {
        //TODO: this class should be part of flow
        /*
        [Inject]
        public LevelCreatorECS(DataModel.IModelProvider modelProvider, LocalData.PrefabProvider prefabProvider)
        {
            EntityManager entityManager = World.Active.EntityManager;

            //TODO: create level selection
            EntityArchetype archetype = entityManager.CreateArchetype(
                typeof(Navigation.NavigationComponent),
                typeof(LocalToWorld),
                typeof(CopyTransformToGameObject)
                );
            var level = modelProvider.Levels.GetEnumerator().Current.Value;
            var prefabs = prefabProvider.Citizens.GetRandom(level.CitizenAmount);
            foreach (var p in prefabs)
            {
                var transform = UnityEngine.Object.Instantiate(p).transform;

                var entity = entityManager.CreateEntity(typeof(Navigation.NavigationComponent));
                entityManager.SetComponentData(entity, new Navigation.NavigationComponent(transform));
            }
        }
        */
    }

    class LevelCreatorMono: ILevelCreator
    {
        [Inject]
        Navigation.INavigationSystem _navigationSystem;
        [Inject]
        IScheduleSystem _scheduleSystem;
        [Inject]
        LocalData.PrefabProvider _prefabProvider;
        UnityEngine.Transform _citizenRoot;
        UnityEngine.AI.NavMeshAgent[] _navMeshAgents;
        

        public SceneData Create(DataModel.LevelModel levelModel)
        {
            var sceneData = LoadSceneData();
            CreateCitizens(levelModel, sceneData);
            return sceneData;
        }
        
        //HACK
        SceneData LoadSceneData()
        {
            SceneData result = new SceneData();
            var root = UnityEngine.GameObject.Find("POLYGON_City_Demo_Scene").transform;
            var transforms = root.GetComponentsInChildren<UnityEngine.Transform>();
            var houses = transforms.Where(t => t.name.Contains("Apart") && t.name.Contains("oor"))
                .Select(t => t.position);
            var offices = transforms.Where(t => t.parent == root && t.name.Contains("ffi")
                || t.name.Contains("Shop"))
                .Select(t => t.position);
            UnityEngine.Debug.Log(offices.Count());
            result.Houses = houses.ToArray();
            result.Offices = offices.ToArray();

            return result;
        }

        void CreateCitizens(DataModel.LevelModel levelModel, SceneData sceneData)
        {
            //NOTE: we make separate cycle in each submethod instead of single cycle for all submethods
            //because result code is easy to refactor
            CitizenFullComponent[] citizens = new CitizenFullComponent[levelModel.CitizenAmount];
            //TODO: ,aybe citizens data should be generated on backend and provided via IncomeData
            
            for (int i = 0; i < citizens.Length; i++)
            {
                citizens[i].Id = System.Guid.NewGuid();
            }
            
            CreateSchedulesForCitizens(citizens, levelModel.CitizenScheduleSettings);
            SetLocationsToCitizens(citizens, sceneData);
            CreateCitizensNavigationThings(citizens);
            
            for (int i = 0; i < citizens.Length; i++)
            {
                citizens[i].Transform.name += " " + i;
            }
        }

        void CreateSchedulesForCitizens(CitizenFullComponent[] citizens, IncomeData.CitizenScheduleSettings settings)
        {
            _scheduleSystem.Initialize(citizens, settings);
        }

        void SetLocationsToCitizens(CitizenFullComponent[] citizens, SceneData sceneData)
        {
            int houseId = 0;
            for (int i = 0; i < citizens.Length; i++)
            {
                citizens[i].TargetPosition = sceneData.Houses[houseId];
                houseId++;
                if (houseId >= sceneData.Houses.Length)
                    houseId = 0;
            }
        }

        void CreateCitizensNavigationThings(CitizenFullComponent[] citizens)
        {
            if (_citizenRoot == null)
            {
                _citizenRoot = new UnityEngine.GameObject("Citizens").transform;
                //HACK: think about solid scene link
                UnityEngine.SceneManagement.SceneManager.MoveGameObjectToScene(_citizenRoot.gameObject,
                    UnityEngine.SceneManagement.SceneManager.GetSceneByBuildIndex(2));
            }

            var prefabs = _prefabProvider.Citizens.GetRandom(citizens.Length);
            for (int i = 0; i < citizens.Length; i++)
            {
                var transform = UnityEngine.Object.Instantiate(prefabs[i], _citizenRoot).transform;
                citizens[i].Transform = transform;
                transform.gameObject.SetActive(false);
                transform.position = citizens[i].TargetPosition;
                //transform.position = _navigationSystem.GetPointWithinBounds(citizens[i].TargetPosition, 20);
                citizens[i].Agent = transform.GetComponent<UnityEngine.AI.NavMeshAgent>();

                var animation = transform.GetComponent<Navigation.IAnimationController>();
                
                //if (animation == null)
                //    animation = transform.gameObject.AddComponent<Navigation.AnimationController>();
                animation.Move();
            }
            _navigationSystem.SetCitizens(citizens);
        }

        
    }

    class SceneData
    {
        public UnityEngine.Vector3[] Houses,
            Offices,
            Entertainments,
            Parks;
    }
}