﻿using STG.IncomeData;
using System.IO;
using UnityEngine;

namespace STG.Test
{
    public class JsonCreator : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            var parameters = new ParameterData[]
            {
                new ParameterData()
                {
                    Id = System.Guid.NewGuid().ToString()
                },
                new ParameterData()
                {
                    Id = System.Guid.NewGuid().ToString()
                },
                new ParameterData()
                {
                    Id = System.Guid.NewGuid().ToString()
                },
                new ParameterData()
                {
                    Id = System.Guid.NewGuid().ToString()
                }
            };
            int trophyCount = 10;

            var trophies = new TrophyData[trophyCount];
            for (int i = 0; i < trophyCount; i++)
            {
                trophies[i] = new TrophyData()
                {
                    Id = System.Guid.NewGuid().ToString(),
                    ParameterValues = new ParameterValue[]
                    {
                        new ParameterValue { Id = parameters[0].Id, Value = Random.Range(1, 10) },
                        new ParameterValue { Id = parameters[1].Id, Value = Random.Range(1, 10) },
                        new ParameterValue { Id = parameters[2].Id, Value = Random.Range(1, 10) },
                        new ParameterValue { Id = parameters[3].Id, Value = Random.Range(1, 10) }
                    }
                };
            };

            var tropyJson = JsonHelper.ToJson(trophies, true);
            var path = Application.streamingAssetsPath + "/Trophies.json";
            File.WriteAllText(path, tropyJson);
            File.WriteAllText(Application.streamingAssetsPath + "/Parameters.json", JsonHelper.ToJson(parameters, true));
        }
    }
}