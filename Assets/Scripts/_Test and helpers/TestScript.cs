﻿using UnityEngine;

namespace STG
{
    public class TestScript : MonoBehaviour
    {
        [SerializeField]
        InputController _input;
        [SerializeField]
        Camera.CameraController _camera;
        // Start is called before the first frame update
        void Start()
        {
            _camera.Initialize(_input);
        }
    }
}