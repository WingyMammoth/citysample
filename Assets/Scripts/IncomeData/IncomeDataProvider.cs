﻿using System;
using System.Collections.Generic;
using System.IO;

namespace STG.IncomeData
{
    //TODO: make it async (or with callbacks)
    public interface IIncomeDataProvider
    {
        PlayerData Player { get; }
        Dictionary<Guid, ParameterData> Parameters { get; }
        Dictionary<Guid, TrophyData> Trophies { get; }
        Dictionary<Guid, LevelData> Levels { get; }
        Dictionary<Guid, CitizenScheduleSettings> CitizenSchedules { get; }

        void LoadData(Action callback);
    }

    public class IncomeDataProviderTest : IIncomeDataProvider
    {
        const string _parametersFilename = "Parameters.json",
            _trophiesFilename = "Trophies.json",
            _playerFilename = "Player.json",
            _levesFilename = "Levels.json",
            _citizenScheduleSettings = "CitizenScheduleSettings.json";

        public PlayerData Player { get; private set; }
        public Dictionary<Guid, ParameterData> Parameters { get; private set; }
        public Dictionary<Guid, TrophyData> Trophies { get; private set; }
        public Dictionary<Guid, LevelData> Levels { get; private set; }
        public Dictionary<Guid, CitizenScheduleSettings> CitizenSchedules { get; private set; }

        public void LoadData(Action callback)
        {
            Parameters = LoadDataDictionary<ParameterData>(_parametersFilename);
            Trophies = LoadDataDictionary<TrophyData>(_trophiesFilename);
            Levels = LoadDataDictionary<LevelData>(_levesFilename);
            Player = LoadData<PlayerData>(_playerFilename);
            CitizenSchedules = LoadDataDictionary<CitizenScheduleSettings>(_citizenScheduleSettings);
            callback?.Invoke();
        }

        Dictionary<Guid, T> LoadDataDictionary<T>(string filename) where T : BaseData
        {
            try
            {
                var array = JsonHelper.FromJson<T>(File.ReadAllText(UnityEngine.Application.streamingAssetsPath + "/" + filename));
                var result = new Dictionary<Guid, T>(array.Length);
                foreach (var item in array)
                    result[item.Guid] = item;
                return result;
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogException(e);
                return null;
            }
        }

        T LoadData<T>(string filename) where T : BaseData
        {
            return UnityEngine.JsonUtility.FromJson<T>(File.ReadAllText(UnityEngine.Application.streamingAssetsPath + "/" + filename));
        }
    }
}