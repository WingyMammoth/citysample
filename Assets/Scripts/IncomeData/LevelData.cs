﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STG.IncomeData
{
    [System.Serializable]
    public class LevelData: BaseData
    {
        public int CitizenAmount;
        public string CitizenScheduleSettingsId;
    }

    [System.Serializable]
    public class CitizenScheduleSettings: BaseData
    {
        //all values are in virtual hours
        public int StartDelayMinutesMax = 1,
            WorkTimeMinutesMin,
            WorkTimeMinutesMax,
            BreakTimeMinutesMin,
            BreakTimeMinutesMax,
            HangoutTimeMinutesMin,
            HangoutTimeMinutesMax,
            BreaksCountMin = 1,
            BreaksCountMax = 3;
        public float VirtualMinutesInRealSecond;
    }
}