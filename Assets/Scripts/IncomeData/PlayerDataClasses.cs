﻿using System;

namespace STG.IncomeData
{
    [Serializable]
    public class BaseData
    {
        public string Id;
        Guid _guid = Guid.Empty;
        public Guid Guid
        {
            get
            {
                if (_guid == Guid.Empty)
                    _guid = new Guid(Id);
                return _guid;
            }
        }
    }

    [Serializable]
    public class ParameterData : BaseData
    {
    }

    [Serializable]
    public class ParameterValue : BaseData
    {
        public int Value;
    }

    [Serializable]
    public class TrophyData : BaseData
    {
        public ParameterValue[] ParameterValues;
    }

    [Serializable]
    public class PlayerData : BaseData
    {
        public string CurrentTrophyId;
        Guid _currentTriphyGuid = Guid.Empty;
        public Guid CurentTrophyGuid
        {
            get
            {
                if (_currentTriphyGuid == Guid.Empty)
                    _currentTriphyGuid = new Guid(CurrentTrophyId);
                return _currentTriphyGuid;
            }
            set
            {
                _currentTriphyGuid = value;
            }
        }
    }

}