﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace STG.Minigames
{
    public class Figure : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        RectTransform _rectTransform;
        Vector3 _delta;
        Vector3 _startPosition;

        CanvasGroup _canvasGroup;
        //SortingGroup _sortingGroup;

        public int SizeX { get; private set; }
        public int SizeY { get; private set; }
        public bool Placed = false;
        public event System.Action OnStartDrag;

        public static Figure FigureBeingDragged { get; private set; }

        void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            if (_canvasGroup == null)
                _canvasGroup = gameObject.AddComponent<CanvasGroup>();
            //_sortingGroup = GetComponent<SortingGroup>();
            //if (_sortingGroup == null)
            //    _sortingGroup = gameObject.AddComponent<SortingGroup>();
            //_sortingGroup.sortingLayerName = "Figure";

            _rectTransform = GetComponent<RectTransform>();
            _startPosition = _rectTransform.position;
            SizeX = (int)(_rectTransform.sizeDelta.x / 50f + .5f);
            SizeY = (int)(_rectTransform.sizeDelta.y / 50f + .5f);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            //TODO: check for multitouch and multidrag
            _delta = _rectTransform.position - (Vector3)eventData.position;
            _canvasGroup.blocksRaycasts = false;
            //_sortingGroup.sortingOrder = 1;
            FigureBeingDragged = this;
            Placed = false;
            OnStartDrag?.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            _rectTransform.position = _delta + (Vector3)eventData.position;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _canvasGroup.blocksRaycasts = true;
            //_sortingGroup.sortingOrder = 0;
            FigureBeingDragged = null;

            if (!Placed)
                Return();
        }

        public void Return()
        {
            _rectTransform.position = _startPosition;
            //throw new System.NotImplementedException();
        }
    }
}