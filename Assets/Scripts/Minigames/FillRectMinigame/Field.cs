﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace STG.Minigames
{


    public class Field : MonoBehaviour, IDropHandler
    {
        int _sizeX = 6,
        _sizeY = 10;
        bool[,] _cells;

        Vector3 _position;
        Vector3 _cellSize = new Vector3(50, 50);
        Dictionary<Figure, Vector2Int> _placedFigures = new Dictionary<Figure, Vector2Int>(10);

        // Start is called before the first frame update
        void Start()
        {
            var rt = GetComponent<RectTransform>();
            _position = (Vector3)rt.rect.min + rt.position;
            Debug.Log(_position);
            InitField();

            foreach (var figure in transform.GetComponentsInChildren<Figure>())
                TryPlaceFigure(figure);
        }

        void InitField()
        {
            _cells = new bool[_sizeX, _sizeY];
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnDrop(PointerEventData eventData)
        {
            TryPlaceFigure(Figure.FigureBeingDragged);
        }

        void TryPlaceFigure(Figure figure)
        {
            var figureTr = figure.transform;
            var delta = figureTr.position - _position;
            int cellX = (int)(delta.x / _cellSize.x + .5f);
            int cellY = (int)(delta.y / _cellSize.y + .5f);
            if (cellX < 0 || cellY < 0)
                return;

            for (int i = cellX; i < cellX + figure.SizeX; i++)
            {
                if (i == _sizeX)
                    return;
                for (int j = cellY; j < cellY + figure.SizeY; j++)
                    if (j == _sizeY || _cells[i, j])
                        return;
            }

            for (int i = cellX; i < cellX + figure.SizeX; i++)
                for (int j = cellY; j < cellY + figure.SizeY; j++)
                    _cells[i, j] = true;

            figure.OnStartDrag += OnPlacedFigureDragHandler;
            figure.Placed = true;
            _placedFigures[figure] = new Vector2Int(cellX, cellY);
            figureTr.position = _position + new Vector3(cellX * _cellSize.x, cellY * _cellSize.y);
        }

        void OnPlacedFigureDragHandler()
        {
            var figure = Figure.FigureBeingDragged;
            figure.OnStartDrag -= OnPlacedFigureDragHandler;
            figure.Placed = false;
            Vector2Int cell;
            if (!_placedFigures.TryGetValue(figure, out cell))
            {
                Debug.LogError("Field is subscribed on figure's event, but figure is absent in the dictionary.");
                return;
            }

            for (int i = cell.x; i < cell.x + figure.SizeX; i++)
                for (int j = cell.y; j < cell.y + figure.SizeY; j++)
                    _cells[i, j] = false;
        }

        bool IsFilled()
        {
            foreach (var cell in _cells)
                if (!cell)
                    return false;
            return true;
        }
    }
}