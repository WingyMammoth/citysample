﻿using UnityEngine;
using UnityEngine.AI;
using Zenject;
using STG.Level;

namespace STG.Navigation
{
    interface INavigationSystem: ICitizenSystem
    {
        event ActionRef<CitizenFullComponent> OnTargetPositionAchieved;
        void SetCitizens(CitizenFullComponent[] citizens);
        void ValidateTargetPosition(ref CitizenFullComponent citizen);
    }

    //TODO: make an interface
    class NavigationSystemMono : INavigationSystem
    {
        public bool Active { get; set; }
        public event ActionRef<CitizenFullComponent> OnTargetPositionAchieved;
        CitizenFullComponent[] _citizens;
        System.Random _random = new System.Random();
        bool _active = false;

#if DEBUG_NAVIGATION
        System.Collections.Generic.Dictionary<int, Transform> _targetCubes;
#endif

        public void SetCitizens(CitizenFullComponent[] citizens)
        {
            _citizens = citizens;
            _active = true;

#if DEBUG_NAVIGATION
            _targetCubes = new System.Collections.Generic.Dictionary<int, Transform>(_citizens.Length);
            foreach (var c in _citizens)
            {
                var cube = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
                GameObject.Destroy(cube.GetComponent<Collider>());
                _targetCubes.Add(c.Transform.GetInstanceID(), cube);
                _targetCubes[c.Transform.GetInstanceID()].SetParent(c.Transform);

                if (!c.Agent.autoRepath)
                {
                    Debug.Log(c.Agent.autoRepath);
                    c.Agent.autoRepath = true;
                }

            }
#endif

            /*
            for (int i = 0; i < _agents.Length; i++)
            {
                for (int j = 0; j < 95; j++)
                {
                    UpdateAgent(_agents[i]);
                    agents[i].transform.position = _agents[i].destination;
                }
            }
            */
        }

        public void Tick(float deltaTime)
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.P))
                _active = !_active;
            if (!_active)
                return;
            if (_citizens == null)
                return;
            for (int i = 0; i < _citizens.Length; i++)
                UpdateAgent(ref _citizens[i]);
        }

        void UpdateAgent(ref CitizenFullComponent citizen)
        {
            if (citizen.CheckDelayTicks > 0)
            {
                citizen.CheckDelayTicks--;
                if (citizen.CheckDelayTicks == 0)
                    citizen.Agent.enabled = true;
                return;
            }

            if (!citizen.Agent.isActiveAndEnabled)
                return;

            if (citizen.Agent.hasPath)
            {
                citizen.NoPathCounts = 0;
                var delta1 = (citizen.Agent.nextPosition - citizen.PreviousPosition).magnitude;
                var delta2 = citizen.Agent.speed * Time.deltaTime;

                if (citizen.Agent.speed * Time.deltaTime * .05f > (citizen.Transform.position - citizen.PreviousPosition).magnitude)
                //if (citizen.Agent.nextPosition == citizen.PreviousPosition)
                {
                    var agentRestDisctance = (citizen.Agent.pathEndPosition - citizen.Agent.nextPosition).magnitude;
                    Debug.Log("SAME POSITION " + citizen.NoPathCounts
                        + "    " + citizen.Transform.name + "  " + citizen.Transform.GetInstanceID()
                        + "    rest distance = " + agentRestDisctance);
                    //if several agents stacked at target point
                    if (agentRestDisctance < 10)
                    {
                        FinishPath(ref citizen);
                    }
                    else
                    {
                        /*
                        citizen.SetNewAgentTargetPosition(10, 4, -5, 5, -5, 5);
                        citizen.CheckDelayTicks = _random.Next(0, 15);
                        */
                        //citizen.Agent.enabled = false;
                        //HACK: teleport
                        citizen.Agent.enabled = false;
                        //citizen.Transform.Translate(Vector3.forward);
                        citizen.Transform.Translate(new Vector3(_random.Next(-1, 2), 0, _random.Next(-1, 2)));
                        citizen.Agent.enabled = true;
                    }
                }
#if DEBUG_NAVIGATION
                _targetCubes[citizen.Transform.GetInstanceID()].position = citizen.TargetPosition;
#endif
            }
            else
            {
                var agentRestDisctance = (citizen.Agent.pathEndPosition - citizen.Agent.nextPosition).magnitude;
                if (citizen.NoPathCounts > 5)
                {
                    Debug.Log("No path count " + citizen.NoPathCounts
                        + "    " + citizen.Transform.name + "  " + citizen.Transform.GetInstanceID()
                        + "    rest distance = " + agentRestDisctance);
                }
                var agentDeltaMagnitude = (citizen.Agent.pathEndPosition - citizen.TargetPosition).magnitude;
                if (agentRestDisctance < 5)
                {
                    Debug.Log("AgentDeltaMagnitude = " + agentDeltaMagnitude);
                    if (agentDeltaMagnitude < 10)
                    {
                        FinishPath(ref citizen);
                    }
                    // if we tried to avoid obstacle - go back to route
                    else
                    {
                        Debug.Log("UPDATE PATH");
                        //Debug.Log(citizen.Agent.SetDestination(citizen.TargetPosition));
                        var path = citizen.Agent.path;
                        citizen.Agent.CalculatePath(citizen.TargetPosition, path);
                        citizen.Agent.SetPath(path);
                        citizen.CheckDelayTicks = 5;
                        citizen.NoPathCounts++;
#if DEBUG_NAVIGATION
                        _targetCubes[citizen.Transform.GetInstanceID()].position = citizen.TargetPosition;
#endif
                    }
                }
            }

            citizen.PreviousPosition = citizen.Agent.nextPosition;
        }

        public void ValidateTargetPosition(ref CitizenFullComponent citizen)
        {
            NavMeshHit hit;
            //NavMeshDataInstance
            if (NavMesh.SamplePosition(citizen.TargetPosition, out hit, 100.0f, NavMesh.AllAreas))
            {
                citizen.TargetPosition = hit.position;
            }
            else
            {
                Debug.LogError("No navmesh position found for target position " + citizen.TargetPosition);
            }
        }

        /// <summary>
        /// Returns the Vector3 point available to go to
        /// </summary>
        /// <param name="center">center of search, usually the position of an NPC we're looking point for</param>
        /// <param name="range">Max fistance from center to result point</param>
        /// <param name="forward">An NPC should move mostly FORWARD. Center and range give us a sphere, which means ANY direction.</param>
        /// <param name="relativeForwardStep">So we just move the sphere center: result center is: center + forward * range * relativeForwardStep</param>
        /// <returns></returns>
        public Vector3 GetPointWithinBounds(Vector3 center, float range = 300)
        {
            for (int i = 0; i < 30; i++) // navMesh tries from unity tutorial
            {
                for (int j = 0; j < 10; j++)// bounds tries
                {
                    //The only line that differs
                    Vector3 randomPoint = center + Random.insideUnitSphere * range;
                    NavMeshHit hit;
                    //NavMeshDataInstance
                    if (NavMesh.SamplePosition(randomPoint, out hit, 2.0f, NavMesh.AllAreas))
                    {
                        return hit.position;
                    }
                }
            }
            return center;
        }

        public void InitializeComponents(CitizenFullComponent[] citizens)
        {
            throw new System.NotImplementedException();
        }

        void FinishPath(ref CitizenFullComponent citizen)
        {
            citizen.Agent.ResetPath();
            OnTargetPositionAchieved?.Invoke(ref citizen);
        }

    }
}