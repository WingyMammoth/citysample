﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace STG.Level
{
    partial struct CitizenFullComponent
    {
        public Transform Transform;
        public NavMeshAgent Agent;
        public Vector3 PreviousPosition;
        public Vector3 TargetPosition;
        public int CheckDelayTicks;
        public int NoPathCounts;
    }
}