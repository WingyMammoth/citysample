﻿using System;
using UnityEngine;

namespace STG.Navigation
{
    interface IAnimationController
    {
        void Move();
        void Stop();
    }

    public class AnimationController : MonoBehaviour, IAnimationController
    {
        enum VariableType { Bool, Float, Int, Trigger }

        Animator _animator;
        [SerializeField] string _parameterName;
        [SerializeField] VariableType _type;
        [Header("Walk")]
        [SerializeField] bool _boolValueWalk;
        [SerializeField] float _floatValueWalk;
        [SerializeField] int _intValueWalk;
        [Header("Idle")]
        [SerializeField] bool _boolValueIdle;
        [SerializeField] float _floatValueIdle;
        [SerializeField] int _intValueIdle;

        Action _moveAction;
        Action _stopAction;

        void Init()
        {
            _animator = GetComponent<Animator>();
            if (_animator == null)
            {
                Destroy(this);
                return;
            }
            switch (_type)
            {
                case VariableType.Bool:
                    _moveAction = () => _animator.SetBool(_parameterName, _boolValueWalk);
                    _stopAction = () => _animator.SetBool(_parameterName, _boolValueIdle);
                    break;
                case VariableType.Float:
                    _moveAction = () => _animator.SetFloat(_parameterName, _floatValueWalk);
                    _stopAction = () => _animator.SetFloat(_parameterName, _floatValueIdle);
                    break;
                case VariableType.Int:
                    _moveAction = () => _animator.SetInteger(_parameterName, _intValueWalk);
                    _stopAction = () => _animator.SetInteger(_parameterName, _intValueIdle);
                    break;
                case VariableType.Trigger:
                    _moveAction = () => _animator.SetTrigger(_parameterName);
                    _stopAction = () => _animator.SetTrigger(_parameterName);
                    break;
            }

            SetIdle();
        }

        public void Move()
        {
            if (_animator == null)
                Init();
            _moveAction?.Invoke();
        }

        public void Stop()
        {
            _stopAction?.Invoke();
        }

        void SetIdle()
        {
            Stop();
            _animator.SetBool("Grounded", true);
        }
    }
}