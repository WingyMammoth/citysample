﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace STG.Reusable
{
    public class SceneLoader : MonoBehaviour
    {
        [SerializeField]
        int[] _sceneBuildIndexesToLoad;
        Scene _currentScene;
        [SerializeField]
        int _activeScene = 2;

        // Start is called before the first frame update
        private void Start()
        {
            _currentScene = SceneManager.GetActiveScene();

            int sceneCount = _sceneBuildIndexesToLoad.Length;
            SceneManager.sceneLoaded += (scene, mode) =>
            {
                sceneCount--;
                if (sceneCount <= 0)
                {
                    SceneManager.UnloadSceneAsync(_currentScene);
                }

                if (scene.buildIndex == _activeScene)
                    SceneManager.SetActiveScene(scene);
            };

            foreach (var buildIndex in _sceneBuildIndexesToLoad)
            {
                if (buildIndex >= SceneManager.sceneCountInBuildSettings)
                    sceneCount--;
                else
                    SceneManager.LoadSceneAsync(buildIndex, LoadSceneMode.Additive);
                //_loader.LoadSceneAsync(buildIndex, LoadSceneMode.Additive);
            }
            if (sceneCount <= 0)
                SceneManager.UnloadSceneAsync(_currentScene);
        }
    }
}