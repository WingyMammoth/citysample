﻿using UnityEngine;

namespace STG.Reusable
{
    public class MonoBehaviourFabric<T> : IGenericFabric<T> where T : MonoBehaviour
    {
        T _prefab;
        T _instance;

        public MonoBehaviourFabric(T prefab)
        {
            _prefab = prefab;
        }

        public T CreateInstance()
        {
            if (_instance == null)
            {
                _instance = GameObject.Instantiate(_prefab);
                return _instance;
            }
            else
            {
                return GameObject.Instantiate(_instance);
            }
        }
    }
}
