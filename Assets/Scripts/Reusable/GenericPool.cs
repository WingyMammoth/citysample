﻿using System.Collections.Generic;

namespace STG.Reusable
{
    public interface IPool<T>
    {
        ICollection<T> ActiveInstances { get; }
        T Take();
        void Return(T instance);
    }

    public interface IGenericFabric<T>
    {
        T CreateInstance();
    }

    public class GenericPool<T> : IPool<T>
    {
        IGenericFabric<T> _fabric;
        //TODO: maybe use Dictionary or other data structure instead
        LinkedList<T> _activeInstances = new LinkedList<T>();
        Queue<T> _inactiveInstances = new Queue<T>();
        public ICollection<T> ActiveInstances => _activeInstances;
        public GenericPool(IGenericFabric<T> fabric)
        {
            _fabric = fabric;
        }

        public T Take()
        {
            var result = _inactiveInstances.Count == 0 ?
                _fabric.CreateInstance() : _inactiveInstances.Dequeue();
            _activeInstances.AddLast(result);
            return result;
        }

        public void Return(T instance)
        {
            _activeInstances.Remove(instance);
            _inactiveInstances.Enqueue(instance);
        }
    }
}