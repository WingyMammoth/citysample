﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STG
{
    public struct ComponentBase
    {
        public System.Guid Id { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid">if Guid.Empty, Id will be generated</param>
        public ComponentBase(System.Guid guid)
        {
            Id = guid == System.Guid.Empty ? System.Guid.NewGuid() : guid;
        }
    }
}