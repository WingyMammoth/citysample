﻿namespace STG
{
    /// <summary>
    /// Bindable prefabs should inherit this interface
    /// to let Zenject know
    /// which component of prefab to bind
    /// </summary>
    public interface IPrefab
    {
    }
}