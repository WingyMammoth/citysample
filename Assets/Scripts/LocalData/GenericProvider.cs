﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace STG.LocalData
{
    /*
    public interface IGenericDataProvider<T>
    {
        bool TryGetValue(Guid id, out T data);
    }
    */

    public abstract class StringGuidData<T>
    {
        [SerializeField]
        string _id;
        public string Id => _id;
        public abstract T Value { get; }
        public StringGuidData(string id) { _id = id; }
    }

    public class SimpleStringGuidData<T> : StringGuidData<T>
    {
        [SerializeField] T _value;
        public override T Value => _value;

        public SimpleStringGuidData(string id, T value) : base(id)
        {
            _value = value;
        }
    }

    public class GenericProviderScriptableObject<T1, T2> : ScriptableObject where T2 : StringGuidData<T1>
    {
        [SerializeField]
        T2[] _data;
        IDictionary<Guid, T1> _dataDictionary;

        public bool TryGetValue(Guid id, out T1 data)
        {
            InitDictionaryIfNeed();
            return _dataDictionary.TryGetValue(id, out data);
        }

        void InitDictionaryIfNeed()
        {
            if (_dataDictionary != null)
                return;
            _dataDictionary = new Dictionary<Guid, T1>(_data.Length);
            foreach (var d in _data)
                _dataDictionary[new Guid(d.Id)] = d.Value;
        }
    }

    //TODO: use this everywhere instead of GenericProviderScriptableObject
    [Serializable]
    public class GenericProvider<T1, T2> where T2 : StringGuidData<T1>
    {
        [SerializeField]
        T2[] _data;
        IDictionary<Guid, T1> _dataDictionary;
        //TODO: think about seed
        System.Random _random = new System.Random();

        public bool TryGetValue(Guid id, out T1 data)
        {
            InitDictionaryIfNeed();
            return _dataDictionary.TryGetValue(id, out data);
        }

        void InitDictionaryIfNeed()
        {
            if (_dataDictionary != null)
                return;
            _dataDictionary = new Dictionary<Guid, T1>(_data.Length);
            foreach (var d in _data)
                _dataDictionary[new Guid(d.Id)] = d.Value;
        }

        public T1 GetRandom()
        {
            //TODO: think about instantiating right here
            return _data.Length == 0 ? default : _data[_random.Next(_data.Length)].Value;
        }

        public T1[] GetRandom(int amount)
        {
            var result = new T1[amount];
            for (int i = 0; i < amount; i++)
                result[i] = GetRandom();
            return result;
        }
    }
}

