﻿using System;
using UnityEngine;

namespace STG.LocalData
{
    [Serializable]
    public class LocalData : StringGuidData<LocalData>
    {
        [SerializeField]
        string _name,
            //_id,
            _description;
        [SerializeField]
        Sprite _icon;
        //public override string Id => _id;
        public override LocalData Value => this;
        public string Name => _name;
        public string Description => _description;
        public Sprite Icon => _icon;

        public LocalData(string id, string name, string description = null, Sprite icon = null) : base(id)
        {
            _name = name;
            _description = description;
            _icon = icon;
        }
    }

    [CreateAssetMenu(menuName = "STG/LocalDataProvider")]
    public class LocalDataProvider : GenericProviderScriptableObject<LocalData, LocalData> { }
}