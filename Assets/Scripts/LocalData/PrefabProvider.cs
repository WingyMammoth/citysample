﻿using UnityEngine;

namespace STG.LocalData
{
    //NOTE: it's necessary because Unity don't serialize generic classes
    [System.Serializable]
    public class StringGuidGameObjectData : SimpleStringGuidData<GameObject>
    {
        public StringGuidGameObjectData(string id, GameObject value) : base(id, value) { }
    }

    [System.Serializable]
    public class GameObjectProvider : GenericProvider<GameObject, StringGuidGameObjectData> { }

    //TODO: instantiate via prototype (from object that is already instantiated
    [CreateAssetMenu(menuName = "STG/PrefabProvider")]
    public class PrefabProvider : ScriptableObject {
        public enum PrefabType { Citizen }
        public GameObjectProvider Citizens;
    }
}