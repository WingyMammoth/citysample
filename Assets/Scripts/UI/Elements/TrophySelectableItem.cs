﻿using STG.DataModel;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace STG.UI
{
    public class TrophySelectableItem : SelectableItem, IPrefab
    {
        [SerializeField]
        TMP_Text _nameSmall,
            _nameBig;
        [SerializeField]
        Image _iconSmall,
            _iconBig;
        Parameter[] _parameters;

        [SerializeField]
        Button _replaceButton;

        public event System.Action OnReplaceButtonCLick;

        ITrophyModel _model;
        public ITrophyModel Model { get => _model; set { _model = value; Refresh(); } }

        protected override void Initialize()
        {
            _replaceButton?.onClick.AddListener(() => OnReplaceButtonCLick?.Invoke());
        }

        void Refresh()
        {
            _nameBig.text = _nameSmall.text = _model.Name;
            _iconBig.sprite = _iconSmall.sprite = _model.Icon;

            if (_parameters == null)
                _parameters = GetComponentsInChildren<Parameter>();
            int i = 0;
            foreach (var p in _model.Parameters)
            {
                if (i >= _parameters.Length)
                    break;
                if (_parameters[i] != null)
                    _parameters[i++].Set(p);
            }
        }
    }
}