﻿using STG.DataModel;
using TMPro;
using UnityEngine;

namespace STG.UI
{
    public class Parameter : MonoBehaviour
    {
        [SerializeField]
        TMP_Text _name,
            _value,
            _nameValue;

        public void Set(IParameterModel model)
        {
            if (_name != null)
                _name.text = model.Name;
            if (_value != null)
                _value.text = model.Value.ToString();
            if (_nameValue != null)
                _nameValue.text = model.Name + ": " + model.Value.ToString();
        }
    }
}