﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace STG.UI
{
    public class TrophyScreen : MonoBehaviour
    {
        SelectableCollection<TrophySelectableItem> _trophyCollection;

        [SerializeField]
        Transform _trophyListRoot;

        [SerializeField]
        Image _icon;
        [SerializeField]
        TMP_Text _name,
            _description;
        Parameter[] _parameters;

        DataModel.IPlayerModel _playerModel;

        [Inject]
        void Init(DataModel.IPlayerModel playerModel, TrophySelectableItem prefab)
        {
            _playerModel = playerModel;
            var fabric = new Reusable.MonoBehaviourFabric<TrophySelectableItem>(prefab);
            foreach (var trophy in playerModel.Trophies)
            {
                var ti = fabric.CreateInstance();
                ti.transform.SetParent(_trophyListRoot);
                ti.Model = trophy.Value;
                ti.OnReplaceButtonCLick += () => _playerModel.CurrentTrophy = ti.Model;
            }

            _trophyCollection = new SelectableCollection<TrophySelectableItem>(transform, OnSelectHandler, true);
            _parameters = GetComponentsInChildren<Parameter>();
            Renew(playerModel.CurrentTrophy);
        }

        void OnSelectHandler(TrophySelectableItem item)
        {
            Renew(item == null ? _playerModel.CurrentTrophy : item.Model);
        }

        void Renew(DataModel.ITrophyModel model)
        {
            _name.text = model.Name;
            _description.text = model.Description;
            _icon.sprite = model.Icon;
            int i = 0;
            foreach (var p in model.Parameters)
            {
                if (i >= _parameters.Length)
                    break;
                if (_parameters[i] != null)
                    _parameters[i++].Set(p);
            }
        }
    }
}