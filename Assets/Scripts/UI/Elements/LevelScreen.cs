﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Button = UnityEngine.UI.Button;

namespace STG.UI
{
    public class LevelScreen : MonoBehaviour
    {
        [SerializeField]
        Button _startLevelButton;

        [Inject]
        void Init(Level.ILevelManager levelManager)
        {
            //HACK
            if (levelManager != null && _startLevelButton != null)
                _startLevelButton.onClick.AddListener(() => {
                    gameObject.SetActive(false);
                    levelManager.LoadLevel();
                });
        }
    }
}