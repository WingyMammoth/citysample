﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace STG.UI
{
    public abstract class SelectableItem : MonoBehaviour, IPointerClickHandler
    {
        public event Action OnSelected;
        bool _selected;
        [SerializeField]
        RectTransform _defaultView,
            _selectedView;

        void Start()
        {
            Select(false);
            Initialize();
        }

        protected virtual void Initialize() { }

        public void OnPointerClick(PointerEventData eventData)
        {
            //if (_selected)
            //    return;
            if (!_selected)
                Select(true);
            OnSelected?.Invoke();
        }

        public void Deselect()
        {
            Select(false);
        }

        void Select(bool select)
        {
            _selected = select;
            _defaultView.gameObject.SetActive(!select);
            _selectedView.gameObject.SetActive(select);
            var rt = GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(rt.sizeDelta.x, select ? _selectedView.sizeDelta.y : _defaultView.sizeDelta.y);
        }
    }
}