﻿using UnityEngine;

namespace STG.UI
{
    public class SelectableCollection<T> where T : SelectableItem
    {
        T _selectedItem;
        System.Action<T> _handler;
        bool _deselectOnSecondClick;

        public SelectableCollection(Transform root, System.Action<T> handler, bool deselectOnSecondClick = false)
        {
            _deselectOnSecondClick = deselectOnSecondClick;
            _handler = handler;
            foreach (var item in root.GetComponentsInChildren<T>())
                item.OnSelected += () => OnSelectHandler(item);
        }

        public SelectableCollection(T[] items, System.Action<SelectableItem> handler)
        {
            _handler = handler;
            foreach (var item in items)
                item.OnSelected += () => OnSelectHandler(item);
        }

        void OnSelectHandler(T item)
        {
            if (item == _selectedItem)
            {
                if (!_deselectOnSecondClick)
                    return;
                item = null;
            }
            _selectedItem?.Deselect();
            _selectedItem = item;
            _handler(item);
        }

        public void AddItem(T item)
        {
            item.OnSelected += () => OnSelectHandler(item);
        }
    }
}