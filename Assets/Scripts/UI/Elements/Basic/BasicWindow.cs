﻿using UnityEngine;
using UnityEngine.UI;

namespace STG.UI
{
    public class BasicWindow : MonoBehaviour
    {
        [SerializeField]
        Button _closeButton;

        private void Awake()
        {
            _closeButton.onClick.AddListener(() => gameObject.SetActive(false));
        }
    }
}