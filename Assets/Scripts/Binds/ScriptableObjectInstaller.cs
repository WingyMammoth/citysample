﻿using System;
using UnityEngine;

namespace STG.Bind
{
    [CreateAssetMenu(menuName = "STG/Binds/ScriptableObjectInstaller")]
    public class ScriptableObjectInstaller : Zenject.ScriptableObjectInstaller
    {
        [SerializeField]
        ScriptableObject[] _scriptableObjects;

        public override void InstallBindings()
        {
            if (_scriptableObjects != null)
                foreach (var so in _scriptableObjects)
                    TryBind(so);
        }

        //Copypaste from GameInstaller
        void TryBind(ScriptableObject scriptableObject)
        {
            if (scriptableObject != null)
                try
                {
                    Container.Bind(scriptableObject.GetType()).FromInstance(scriptableObject);
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                }
        }
    }
}