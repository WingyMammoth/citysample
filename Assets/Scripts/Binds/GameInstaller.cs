﻿using STG.DataModel;
using STG.IncomeData;
using System;
using UnityEngine;
using Zenject;

namespace STG.Bind
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField]
        MonoBehaviour[] _prefabs;

        public override void InstallBindings()
        {
            IIncomeDataProvider idp = new IncomeDataProviderTest();
            Container.BindInstance(idp).AsSingle();
            idp.LoadData(OnDataLoadedCallback);
            foreach (var p in _prefabs)
                TryBindPrefab(p.GetComponent<IPrefab>());
        }

        void OnDataLoadedCallback()
        {
            Container.Bind<IPlayerModel>().To<PlayerModel>().AsSingle();
            Container.Bind<IModelProvider>().To<ModelProvider>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<STG.Level.LevelManager>().AsSingle();

            var otherInstallers = GetComponents<MonoInstaller>();
            //Copypaste from Context.cs
            foreach (var installer in otherInstallers)
                if (installer != this)
                {
                    ModestTree.Assert.IsNotNull(installer,
                    "Found null installer in '{0}'", GetType());

                    Container.Inject(installer);

#if ZEN_INTERNAL_PROFILING
                using (ProfileTimers.CreateTimedBlock("User Code"))
#endif
                    {
                        installer.InstallBindings();
                    }
                }

        }

        void TryBindPrefab<T>(T prefab) where T : IPrefab
        {
            if (prefab != null)
                try
                {
                    Container.Bind(prefab.GetType()).FromInstance(prefab);
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                }
        }
    }
}