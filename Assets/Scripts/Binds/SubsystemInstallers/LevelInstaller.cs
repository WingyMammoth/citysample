﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using STG.DataModel;
using STG.Level;

namespace STG.Bind
{
    public class LevelInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<Navigation.INavigationSystem>().To<Navigation.NavigationSystemMono>().AsSingle();
            Container.Bind<ILevelCreator>().To<LevelCreatorMono>().AsSingle();
            Container.Bind<IScheduleSystem>().To<CitizenScheduleSystem>().AsSingle();
            Container.Bind<IHangoutSystem>().To<CitizenHangoutSystem>().AsSingle();
        }
    }
}