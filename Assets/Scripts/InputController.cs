﻿using System;
using UnityEngine;

namespace STG
{
    public class InputController : MonoBehaviour, STG.Camera.IInput
    {
        [SerializeField]
        bool _invertVerticalAxis;

        public event Action<bool> OnForward;
        public event Action<bool> OnBackward;
        public event Action<bool> OnLeft;
        public event Action<bool> OnRight;
        //mouse
        public event Action<float> OnScroll;
        public event Action<float> OnMouseMoveX;
        public event Action<float> OnMouseMoveY;
        public event Action<bool> OnLMB;
        public event Action<bool> OnRMB;
        // Update is called once per frame

        Vector3 _previousMousePosition;

        void OnEnable()
        {
            _previousMousePosition = Input.mousePosition;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.W))
                OnForward?.Invoke(true);
            else if (Input.GetKeyUp(KeyCode.W))
                OnForward?.Invoke(false);
            if (Input.GetKeyDown(KeyCode.S))
                OnBackward?.Invoke(true);
            else if (Input.GetKeyUp(KeyCode.S))
                OnBackward?.Invoke(false);
            if (Input.GetKeyDown(KeyCode.A))
                OnLeft?.Invoke(true);
            else if (Input.GetKeyUp(KeyCode.A))
                OnLeft?.Invoke(false);
            if (Input.GetKeyDown(KeyCode.D))
                OnRight?.Invoke(true);
            else if (Input.GetKeyUp(KeyCode.D))
                OnRight?.Invoke(false);

            var scroll = Input.mouseScrollDelta.y;
            if (scroll != 0)
                OnScroll?.Invoke(-scroll);

            var mouseDelta = Input.mousePosition - _previousMousePosition;
            if (mouseDelta.x != 0)
            {
                OnMouseMoveX?.Invoke(mouseDelta.x);
                _previousMousePosition = Input.mousePosition;
            }
            if (mouseDelta.y != 0)
            {
                OnMouseMoveY?.Invoke(_invertVerticalAxis ? -mouseDelta.y : mouseDelta.y);
                _previousMousePosition = Input.mousePosition;
            }
            if (Input.GetMouseButtonDown(0))
                OnLMB?.Invoke(true);
            else if (Input.GetMouseButtonUp(0))
                OnLMB?.Invoke(false);
            if (Input.GetMouseButtonDown(1))
                OnRMB?.Invoke(true);
            else if (Input.GetMouseButtonUp(1))
                OnRMB?.Invoke(false);
        }
    }
}