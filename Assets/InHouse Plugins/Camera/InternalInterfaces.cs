﻿using UnityEngine;

namespace STG.Camera
{
    interface ICameraController
    {
        void Initialize(IInput input, VirtualCameraTransform virtualCameraTransform, CameraConfig config, AnchorLinkSettings settings);
        void UpdateTransform(float deltaTime);
    }

    interface ICameraStateController
    {
        VirtualCameraTransform VirtualCameraTransform { get; }
        void Initialize(IInput input, CameraConfig config, AnchorLinkSettings settings);
        void UpdateTransform(float deltaTime);
    }

    interface IFollowTransformController
    {
        /// <summary>
        /// Tries to set a transform to follow.
        /// Returns CameraState.FollowTransform for success
        /// and previousState for failure (works as "stop follow transform" case)
        /// </summary>
        /// <param name="target">transform to follow</param>
        /// <param name="previousState">state to return for failure</param>
        /// <param name="forced">true for following same target again;
        /// if false, same target works as "unfollow"</param>
        /// <returns>State to switch camera to</returns>
        CameraState TrySetNewTargetTransform(Transform target, CameraState previousState, bool forced = false);
    }

    interface IBlendController
    {
        //event Action OnBlendFinished;
        bool IsBlending { get; }
        VirtualCameraTransform VirtualCameraTransform { get; }
        void Initialize(BlendConfig defaultConfig);
        void Blend(VirtualCameraTransform from, VirtualCameraTransform to, BlendConfig customConfig = null);
        void Update(float deltaTime);
    }
}
