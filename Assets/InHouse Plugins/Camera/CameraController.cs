﻿using UnityEngine;

namespace STG.Camera
{
    public class CameraController : MonoBehaviour, ICamera
    {
        [SerializeField]
        CameraConfig _config;
        [SerializeField]
        AnchorLinkSettings _anchorLinkSettings;
        [SerializeField]
        CameraStateController[] _stateBehaviors;
        [SerializeField]
        BlendConfig _defaultBlend;
        [SerializeField]
        BlendConfig[] _customBlends;

        CameraState _state = CameraState.Free;
        CameraStateController _currentStateBehaviors = null;
        Transform _transform;
        IFollowTransformController _followTransformController;
        IBlendController _blendController = new BlendController();

        public void Initialize(IInput input)
        {
            _transform = transform;
            for (int i = 0; i < _stateBehaviors.Length; i++)
            {
                if (_stateBehaviors[i] != null)
                {
                    _stateBehaviors[i] = Instantiate(_stateBehaviors[i]);
                    _stateBehaviors[i].Initialize(input, _config, _anchorLinkSettings);
                }
            }

            foreach (var b in _stateBehaviors)
                if (b.State == CameraState.FollowTransform)
                {
                    foreach (var c in b.Controllers)
                    {
                        _followTransformController = c as IFollowTransformController;
                        if (_followTransformController != null)
                            break;
                    }
                    break;
                }
            _blendController.Initialize(_defaultBlend);

            SetState(CameraState.Free);
        }

        // Update is called once per frame
        void Update()
        {
            if (_currentStateBehaviors == null)
                return;

            var dt = Time.deltaTime;
            _currentStateBehaviors.UpdateTransform(dt);

            if (_blendController.IsBlending)
            {
                _blendController.Update(dt);
                _transform.position = _blendController.VirtualCameraTransform.Position;
                _transform.rotation = _blendController.VirtualCameraTransform.Rotation;
            }
            else
            {
                _transform.position = _currentStateBehaviors.VirtualCameraTransform.Position;
                _transform.rotation = _currentStateBehaviors.VirtualCameraTransform.Rotation;
            }
        }

        void SetState(CameraState newState)
        {
            if (_state == newState && _currentStateBehaviors != null)
                return;
            VirtualCameraTransform from = _currentStateBehaviors == null ?
                null : _currentStateBehaviors.VirtualCameraTransform;

            _currentStateBehaviors = null;
            foreach (var b in _stateBehaviors)
                if (b.State == newState)
                {
                    _currentStateBehaviors = b;
                    //blend
                    foreach (var blend in _customBlends)
                    {
                        //from _state to state or
                        //from _state to any or
                        //from any to state
                        if ((blend.From == _state && (blend.To == newState || blend.To == CameraState._Any_))
                            || (blend.From == CameraState._Any_ && blend.To == newState))
                        {
                            _blendController.Blend(from, _currentStateBehaviors.VirtualCameraTransform, blend);
                            break;
                        }
                    }
                    break;
                }
            if (!_blendController.IsBlending)
                _blendController.Blend(from, _currentStateBehaviors.VirtualCameraTransform);
            _state = newState;
        }

        public void FollowTransform(Transform target, bool forced = false)
        {
            if (_followTransformController != null)
                SetState(_followTransformController.TrySetNewTargetTransform(target, _state, forced));
        }
    }
}