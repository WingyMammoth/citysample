﻿using System;

namespace STG.Camera
{
    public interface ICamera
    {
        void Initialize(IInput input);
        /// <summary>
        /// Make camera follow tranform or make camera free if transform is null
        /// </summary>
        /// <param name="transform">transform to follow, null to set camera free</param>
        void FollowTransform(UnityEngine.Transform transform, bool forced = false);
    }

    public interface IInput
    {
        event Action<bool> OnForward,
            OnBackward,
            OnLeft,
            OnRight;
        event Action<float> OnScroll;
        event Action<float> OnMouseMoveX;
        event Action<float> OnMouseMoveY;
        event Action<bool> OnLMB;
        event Action<bool> OnRMB;
    }
}