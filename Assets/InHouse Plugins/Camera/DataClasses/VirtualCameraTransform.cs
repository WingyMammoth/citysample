﻿using UnityEngine;

namespace STG.Camera
{
    class VirtualCameraTransform
    {
        public Vector3 Position;
        public Quaternion Rotation;
        //public float HorizontalRotation;
        //public float VerticalRotation;
    }
}
