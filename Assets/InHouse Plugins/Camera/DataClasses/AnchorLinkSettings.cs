﻿using System;
using UnityEngine;

namespace STG.Camera
{
    [Serializable]
    class AnchorLinkSettings
    {
        public Transform FollowTarget;
        public Vector3 FollowOffsetMax;
        public Transform LookAtTarget;
        //public Vector3 LookAtOffset;
        [Range(0, 1)]
        public float ZoomValueMin;
        [NonSerialized]
        public Vector3 FollowOffsetCurrent;
        [NonSerialized]
        public bool IsChanged;

        public void Override(AnchorLinkSettings source)
        {
            FollowTarget = source.FollowTarget;
            FollowOffsetMax = source.FollowOffsetMax;
            LookAtTarget = source.LookAtTarget;
            ZoomValueMin = source.ZoomValueMin;
        }

        public void Update(AnchorLinkSettings source)
        {
            if (FollowTarget == null)
                FollowTarget = source.FollowTarget;
            if (LookAtTarget == null)
                LookAtTarget = source.LookAtTarget;
        }
    }
}
