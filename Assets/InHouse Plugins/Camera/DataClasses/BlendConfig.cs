﻿using System;
using UnityEngine;

namespace STG.Camera
{
    [Serializable]
    class BlendConfig
    {
        public enum BlendType { Transit, Fade }
        public CameraState From;
        public CameraState To;
        public BlendType Type;
        [Header("Transit settings")]
        public float TransitTime = 1.5f;
        [Header("Fade blend settings")]
        public float FadeInTime = .7f;
        public float FadeTime = .6f;
        public float FadeOutTime = .7f;
    }
}
