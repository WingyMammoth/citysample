﻿using UnityEngine;

namespace STG.Camera
{
    enum CameraState { _Any_, Free, FollowTransform }

    [CreateAssetMenu(menuName = "STG/Camera/StateBehaviors")]
    class CameraStateController : ScriptableObject, ICameraStateController
    {
        public VirtualCameraTransform VirtualCameraTransform { get; }
            = new VirtualCameraTransform();

        public CameraState State;
        public AbstractTransformController[] Controllers;

        [SerializeField]
        bool _overrideSettings;
        [SerializeField]
        private AnchorLinkSettings _anchorLinkSettings;

        public void Initialize(IInput input, CameraConfig config, AnchorLinkSettings settings)
        {
            //if not override settings,
            //we use not SELF settings, but INCOMING settings
            //that's why we call _anchorLingSettings.Override() if _overrideSettings is false
            //but we don't reference to the original settings object - 
            //each state should manipulate its own settings object
            if (!_overrideSettings)
                _anchorLinkSettings.Override(settings);
            else
                _anchorLinkSettings.Update(settings);

            for (int i = 0; i < Controllers.Length; i++)
            {
                if (Controllers[i] != null)
                {
                    Controllers[i] = Instantiate(Controllers[i]);
                    Controllers[i].Initialize(input, VirtualCameraTransform, config, _anchorLinkSettings);
                }
            }
            _anchorLinkSettings.FollowOffsetCurrent = _anchorLinkSettings.FollowOffsetMax;
            if (State == CameraState._Any_)
                State = CameraState.Free;
        }

        public void UpdateTransform(float deltaTime)
        {
            _anchorLinkSettings.IsChanged = false;
            foreach (var c in Controllers)
                c.UpdateTransform(deltaTime);
            if (_anchorLinkSettings.IsChanged)
            {
                VirtualCameraTransform.Position = _anchorLinkSettings.FollowTarget.position +
                    _anchorLinkSettings.LookAtTarget.TransformVector(_anchorLinkSettings.FollowOffsetCurrent);
                VirtualCameraTransform.Rotation = Quaternion.LookRotation(
                    _anchorLinkSettings.LookAtTarget.position - VirtualCameraTransform.Position);
            }
        }
    }
}
