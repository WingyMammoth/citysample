﻿using UnityEngine;

namespace STG.Camera
{
    [CreateAssetMenu(menuName = "STG/Camera/Config")]
    class CameraConfig : ScriptableObject
    {
        public float HorizontalMovementSpeed = 2;

        [Header("Zoom")]
        public float ZoomInputMultiplier = 10;
        public float ZoomMaximumSpeed = 10;
        public float ZoomMaximumInputSum = 10;
        public float ZoomDecreaseMultiplier = 3;

        [Header("Rotation")]
        public float HorizontalRotationSpeed = 1;
        public float VerticalRotationSpeed = 1;
        public float VerticalAngleMax = 70;
    }
}
