﻿namespace STG.Camera
{
    [UnityEngine.CreateAssetMenu(menuName = "STG/Camera/Controllers/Vertical Rotate")]
    class VerticalRotateController : AbstractTransformController
    {
        bool _rmbPressed;
        float _input;
        UnityEngine.Vector3 _right = UnityEngine.Vector3.right;

        public override void UpdateTransform(float deltaTime)
        {
            if (!_rmbPressed || _input == 0)
                return;
            var currentAngle = _settings.LookAtTarget.eulerAngles.x;
            var deltaAngle = _input * _config.VerticalRotationSpeed;
            deltaAngle = -currentAngle +
                System.Math.Max(0, System.Math.Min(_config.VerticalAngleMax, currentAngle + deltaAngle));
            if (deltaAngle == 0)
                return;
            var q = UnityEngine.Quaternion.AngleAxis(deltaAngle, _right);
            _input = 0;
            _settings.LookAtTarget.rotation *= q;
            _settings.IsChanged = true;
        }

        protected override void CustomInitialize(IInput input, VirtualCameraTransform cameraTransform)
        {
            input.OnRMB += value => _rmbPressed = value;
            input.OnMouseMoveY += value => _input = value;
        }
    }
}
