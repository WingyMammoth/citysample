﻿using System;

namespace STG.Camera
{
    class BlendController : IBlendController
    {
        //public event Action OnBlendFinished;
        public bool IsBlending { get; private set; }
        public VirtualCameraTransform VirtualCameraTransform { get; }
            = new VirtualCameraTransform();
        BlendConfig _defaultConfig;
        BlendConfig _currentConfig;
        float _restTime;
        Action<float> UpdateDelegate;
        VirtualCameraTransform _targetTransform;

        public void Update(float deltaTime)
        {
            if (!IsBlending)
                return;
            if (_restTime < deltaTime)
            {
                VirtualCameraTransform.Position = _targetTransform.Position;
                VirtualCameraTransform.Rotation = _targetTransform.Rotation;
                IsBlending = false;
                return;
            }
            _restTime -= deltaTime;
            UpdateDelegate(deltaTime);
        }

        void UpdateTransit(float deltaTime)
        {
            var k = deltaTime / _restTime;
            VirtualCameraTransform.Position = UnityEngine.Vector3.Lerp(
                VirtualCameraTransform.Position, _targetTransform.Position, k);
            VirtualCameraTransform.Rotation = UnityEngine.Quaternion.Lerp(
                VirtualCameraTransform.Rotation, _targetTransform.Rotation, k);
        }

        void UpdateFade(float deltaTime)
        {

        }

        public void Initialize(BlendConfig defaultConfig)
        {
            _defaultConfig = defaultConfig;
            _currentConfig = _defaultConfig;
        }

        public void Blend(VirtualCameraTransform from, VirtualCameraTransform to, BlendConfig customConfig = null)
        {
            if (customConfig != null)
                _currentConfig = customConfig;

            IsBlending = true;
            switch (_currentConfig.Type)
            {
                case BlendConfig.BlendType.Fade:
                    UpdateDelegate = UpdateFade;
                    break;
                default:
                    _restTime = _currentConfig.TransitTime;
                    UpdateDelegate = UpdateTransit;
                    break;
            }
            _targetTransform = to;
            if (from == null)
            {
                _restTime = 0;
            }
            else
            {
                VirtualCameraTransform.Position = from.Position;
                VirtualCameraTransform.Rotation = from.Rotation;
            }
        }
    }
}
