﻿using UnityEngine;

namespace STG.Camera
{
    [CreateAssetMenu(menuName = "STG/Camera/Controllers/Follow Transform")]
    class FollowTransformController : AbstractTransformController, IFollowTransformController
    {
        Transform _target;
        CameraState _previousState;
        Vector3 _prevPos;

        protected override void CustomInitialize(IInput input, VirtualCameraTransform cameraTransform)
        {
            //throw new System.NotImplementedException();
        }

        public override void UpdateTransform(float deltaTime)
        {
            if (_target != null && _prevPos != _target.position)
            {
                _settings.FollowTarget.position = _target.position;
                _settings.IsChanged = true;
            }
        }

        public CameraState TrySetNewTargetTransform(Transform target, CameraState previousState, bool forced = false)
        {
            var success = target != null && (target != _target || forced);
            _target = success ? target : null;
            if (previousState != CameraState.FollowTransform)
                _previousState = previousState;
            return success ? CameraState.FollowTransform : _previousState;
        }
    }
}