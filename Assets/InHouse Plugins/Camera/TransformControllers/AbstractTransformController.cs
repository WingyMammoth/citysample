﻿using UnityEngine;

namespace STG.Camera
{
    abstract class AbstractTransformController : ScriptableObject, ICameraController
    {
        protected AnchorLinkSettings _settings;
        protected CameraConfig _config;

        public void Initialize(IInput input, VirtualCameraTransform cameraTransform,
            CameraConfig config, AnchorLinkSettings settings)
        {
            _settings = settings;
            _config = config;
            CustomInitialize(input, cameraTransform);
        }

        protected abstract void CustomInitialize(IInput input, VirtualCameraTransform cameraTransform);
        public abstract void UpdateTransform(float deltaTime);
    }
}
