﻿using UnityEngine;

namespace STG.Camera
{
    [CreateAssetMenu(menuName = "STG/Camera/Controllers/Horizontal Move")]
    class HorizontalMoveController : AbstractTransformController
    {
        int _forward = 0,
            _right = 0;
        Vector3 _previousPosition;

        protected override void CustomInitialize(IInput input, VirtualCameraTransform cameraTransform)
        {
            _forward = 0;
            _right = 0;
            input.OnForward += pressed => _forward += pressed ? _forward > 0 ? 0 : 1 : -1;
            input.OnBackward += pressed => _forward += pressed ? _forward < 0 ? 0 : -1 : 1;
            input.OnRight += pressed => _right += pressed ? _right > 0 ? 0 : 1 : -1;
            input.OnLeft += pressed => _right += pressed ? _right < 0 ? 0 : -1 : 1;
        }

        public override void UpdateTransform(float deltaTime)
        {
            if (_forward == 0 && _right == 0)
            {
                _settings.IsChanged = _previousPosition != _settings.FollowTarget.position;
            }
            else
            {

                _settings.FollowTarget.Translate(new Vector3(_right, 0, _forward) * _config.HorizontalMovementSpeed * deltaTime);
                _settings.IsChanged = true;
                _previousPosition = _settings.FollowTarget.position;
            }
        }
    }
}