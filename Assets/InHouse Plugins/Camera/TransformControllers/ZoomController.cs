﻿using System;
using UnityEngine;

namespace STG.Camera
{
    [CreateAssetMenu(menuName = "STG/Camera/Controllers/Zoom")]
    class ZoomController : AbstractTransformController
    {
        float _inputSum;
        float _currentZoom;
        float _zoomMagnitude;

        protected override void CustomInitialize(IInput input, VirtualCameraTransform cameraTransform)
        {
            _inputSum = 0;
            input.OnScroll += v => _inputSum = Math.Max(-_config.ZoomMaximumInputSum,
                Math.Min(_config.ZoomMaximumInputSum, v * _config.ZoomInputMultiplier));
            _zoomMagnitude = (cameraTransform.Position - _settings.FollowTarget.position).magnitude;
            _currentZoom = _zoomMagnitude / _settings.FollowOffsetMax.magnitude;
            _currentZoom = Math.Max(_settings.ZoomValueMin, Math.Min(1, _currentZoom));
            _settings.FollowOffsetCurrent = _settings.FollowOffsetMax * _currentZoom;
        }

        public override void UpdateTransform(float deltaTime)
        {
            if (_inputSum == 0)
                return;
            _settings.IsChanged = true;
            var delta = Math.Sign(_inputSum) *
                Math.Min(Math.Abs(_inputSum), Math.Abs(_config.ZoomMaximumSpeed)) * deltaTime;
            var decrease = delta * _config.ZoomDecreaseMultiplier;
            if (Math.Abs(decrease) > Math.Abs(_inputSum))
            {
                delta = _inputSum;
                _inputSum = 0;
            }
            else
            {
                _inputSum -= decrease;
            }
            delta /= _zoomMagnitude;
            _currentZoom += delta;
            if (_currentZoom > 1)
            {
                _currentZoom = 1;
                _inputSum = 0;
            }
            else if (_currentZoom < _settings.ZoomValueMin)
            {
                _currentZoom = _settings.ZoomValueMin;
                _inputSum = 0;
            }
            _settings.FollowOffsetCurrent = _settings.FollowOffsetMax * _currentZoom;
        }
    }
}
