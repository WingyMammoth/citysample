﻿namespace STG.Camera
{
    [UnityEngine.CreateAssetMenu(menuName = "STG/Camera/Controllers/Horizontal Rotate")]
    class HorizontalRotateController : AbstractTransformController
    {
        bool _rmbPressed;
        float _input;
        UnityEngine.Vector3 _up = UnityEngine.Vector3.up;

        public override void UpdateTransform(float deltaTime)
        {
            if (!_rmbPressed || _input == 0)
                return;
            var q = UnityEngine.Quaternion.AngleAxis(_input * _config.HorizontalRotationSpeed, _up);
            _input = 0;
            _settings.FollowTarget.rotation *= q;
            _settings.IsChanged = true;
        }

        protected override void CustomInitialize(IInput input, VirtualCameraTransform cameraTransform)
        {
            input.OnRMB += value => _rmbPressed = value;
            input.OnMouseMoveX += value => _input = value;
        }
    }
}
